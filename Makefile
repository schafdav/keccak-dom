
# Following directories are searched by tools for includes/modules
INCDIRS:=src/rtl
SRCDIRS:=src/rtl

# List of Verilog parameters
PARAMS:= RATE=1088
PARAMS+= W=64
PARAMS+= SHARES=2
PARAMS+= ABSORB_LANES=17 # == RATE/W
PARAMS+= RESET_ITERATIVE=1
PARAMS+= ABSORB_ITERATIVE=1
PARAMS+= THETA_ITERATIVE=1
PARAMS+= RHO_PI_ITERATIVE=0
PARAMS+= CHI_IOTA_ITERATIVE=1
PARAMS+= SLICES_PARALLEL=1
PARAMS+= CHI_DOUBLE_CLK=0
PARAMS+= LESS_RAND=1
PARAMS+= DOM_PIPELINE=1

# List of defines
DEFINES:=

TB:=src/tb/keccak_tb.cpp
TOP:=src/rtl/keccak_top.v


ARG_DEFINES:=$(patsubst %,-D%,$(DEFINES))
ARG_INCDIRS:=$(patsubst %,+incdir+%,$(INCDIRS))
ARG_SRCDIRS:=$(patsubst %,-y %,$(INCDIRS))

IVERILOG_PARAMS:=$(patsubst %,-P%,$(PARAMS))
VERILATOR_PARAMS:=$(patsubst %,-G%,$(PARAMS))

.PHONY: clean help simulate verilate synthesize

help:
	@echo "Possible targets:"
	@echo "	clean      - Clean everything"
	@echo "	help       - This help text"
	@echo "	simulate   - Run simulation"
	@echo "	synthesize - Run synthesis"
	@echo "	verilate   - Run simulation using Verilator"
	@echo "	vhier      - Show a hierarchical view of the design"

clean:
	rm -r build/sim

testfiles:
	mkdir -p build/sim/testfiles
	python src/hlm/main.py create-tests --rate=1088 --capacity=512 --shares=1 --directory=build/sim/testfiles
	python src/hlm/main.py create-tests --rate=1088 --capacity=512 --shares=2 --directory=build/sim/testfiles
	python src/hlm/main.py create-tests --rate=1088 --capacity=512 --shares=3 --directory=build/sim/testfiles
	python src/hlm/main.py create-tests --rate=1088 --capacity=512 --shares=4 --directory=build/sim/testfiles
	python src/hlm/main.py create-tests --rate=1088 --capacity=512 --shares=5 --directory=build/sim/testfiles
	python src/hlm/main.py create-tests --rate=128  --capacity=272 --shares=1 --directory=build/sim/testfiles
	python src/hlm/main.py create-tests --rate=128  --capacity=272 --shares=2 --directory=build/sim/testfiles
	python src/hlm/main.py create-tests --rate=128  --capacity=272 --shares=3 --directory=build/sim/testfiles
	python src/hlm/main.py create-tests --rate=128  --capacity=272 --shares=4 --directory=build/sim/testfiles
	python src/hlm/main.py create-tests --rate=128  --capacity=272 --shares=5 --directory=build/sim/testfiles

simulate-xsim: testfiles
	mkdir -p build/sim/xsim
	ln -sf $(PWD)/build/sim/testfiles build/sim/xsim/testfiles
	ln -sf $(PWD)/src build/sim/xsim/
	cd build/sim/xsim && \
		xvlog --sv -m64 --work work --relax src/rtl/{keccak_chi_iota.v,keccak_theta.v,keccak_state.v,keccak_sbox.v,keccak_roundconstant.v,keccak_rhopi.v,keccak_pi.v,keccak_control.v,keccak_top.v} src/tb/keccak_tb.sv && \
		xelab --debug typical --relax -L work --snapshot keccak_tb_behav work.keccak_tb -log elaborate.log && \
		xsim keccak_tb_behav --runall --log simulate.log

# Icarus is a little stupid when it comes to testbench code
simulate:
	@echo "Not yet functional - use simulate-xsim"
	exit 1
	iverilog -Wall -g2012 \
		$(ARG_DEFINES) \
		$(IVERILOG_PARAMS) \
		$(ARG_INCDIRS) \
		$(ARG_SRCDIRS) \
		$(TOP)

verilate:
	@echo "Not yet functional"
	exit 1
	mkdir -p build/sim/verilator
	ln -sf $(PWD)/src build/sim/verilator/
	cd build/sim/verilator && \
	verilator \
		-Wall -Wno-WIDTH -Wno-VARHIDDEN -Wno-LITENDIAN -Wno-UNUSED -Wno-CASEINCOMPLETE -Wno-UNSIGNED --assert -O2 \
		$(ARG_DEFINES) \
		$(VERILATOR_PARAMS) \
		$(ARG_INCDIRS) \
		$(ARG_SRCDIRS) \
		--cc $(TOP) \
		--exe $(TB)
	$(MAKE) -j4 -C obj_dir -f Vkeccak_top.mk Vkeccak_top
	obj_dir/Vkeccak_top

synthesize:
	yosys scripts/synth.ys

vhier:
	vhier $(ARG_DEFINES) $(ARG_INCDIRS) $(ARG_SRCDIRS) --forest $(TOP)

