# Keccak - DOM

A versatile Keccak implementation.
It uses the Domain Oriented Masking scheme to achieve resilience against side-channel attacks.
