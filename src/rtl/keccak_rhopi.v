`timescale 1ns/1ns

module keccak_rhopi #(
  parameter W = 16
)(
  input wire[25*W-1:0] State_DI,
  output reg[25*W-1:0] State_DO
);

function integer Idx(input integer x, input integer y);
  Idx = (5*x+y)*W;
endfunction

localparam [25*8-1:0] ROTATION_OFFSETS = {
   8'd14, 8'd08, 8'd39, 8'd20, 8'd27,
   8'd56, 8'd21, 8'd25, 8'd55, 8'd28,
   8'd61, 8'd15, 8'd43, 8'd06, 8'd62,
   8'd02, 8'd45, 8'd10, 8'd44, 8'd01,
   8'd18, 8'd41, 8'd03, 8'd36, 8'd00
};

`define shift (W - (ROTATION_OFFSETS[(5*x+y)*8 +: 8] % W))

always @(*) begin : RHO_PI_COMB
  reg[W-1:0] lane;
  integer x, y;

  for(x=0; x < 5; x=x+1) begin
    for(y=0; y < 5; y=y+1) begin
      //rho[Idx(x,y) +: W] = {2{State_DI[Idx(x,y) +: W]}} >> `shift;

      lane = {2{State_DI[Idx(x,y) +: W]}} >> `shift;
      State_DO[Idx(y,(2*x+3*y)%5) +: W] = lane;
    end
  end
end

//function reg[W-1:0] rotate(input reg[W-1:0] lane, input integer amount);
//  reg[2*W-1:0] tmp;
//  rotate = {lane[Idx(x,y) + (W-amount) +: amount],
//        lane[Idx(x,y) + amount +: W-amount]};
//endfunction
//
//
//generate
//  reg[25*W-1:0] rho;
//  genvar x, y;
//
//  for(x=0; x < 5; x=x+1) begin
//    for(y=0; y < 5; y=y+1) begin
//      //rho[Idx(x,y) +: W] = {2{State_DI[Idx(x,y) +: W]}} >> `shift;
//      assign rho[Idx(x,y) +: W] = {
//        State_DI[Idx(x,y) + (W-`shift) +: `shift],
//        State_DI[Idx(x,y) + `shift +: W-`shift]};
//      assign State_DO[Idx(y,(2*x+3*y)%5) +: W] = rho[Idx(x,y) +: W];
//    end
//  end
//endgenerate

endmodule
