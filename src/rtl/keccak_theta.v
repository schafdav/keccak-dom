`timescale 1ns/1ns

module keccak_theta #(
  parameter W = 16,
  parameter SLICES_PARALLEL = 1
)(
  input wire Clk_CI,
  input wire Rst_RBI,
  input wire RstSync_RI,
  input wire Enable_SI,
  input wire[25*SLICES_PARALLEL-1:0] Slices_DI,
  input wire[24:0] SliceZ0_DI,
  output reg[25*SLICES_PARALLEL-1:0] Slices_DO,
  output reg[24:0] SliceZ0_DO
);

function integer Idx(input integer x, input integer y);
  Idx = (5*x+y)*SLICES_PARALLEL;
endfunction

reg[5*SLICES_PARALLEL-1:0] C, C_rot;
reg[25*SLICES_PARALLEL-1:0] A, D;
localparam SP = SLICES_PARALLEL;

  //-----------------------------------------------------------------------------
  // Fully parallel Theta step. Computed in 1 cycle
generate if (W == SLICES_PARALLEL) begin

  always @(*) begin : THETA_PARALLEL
    integer x, y;
    A = Slices_DI;
    for(x=0; x < 5; x=x+1) begin
      C[x*SP +: SP] = A[Idx(x,0) +: SP] ^ A[Idx(x,1) +: SP] ^ A[Idx(x,2) +: SP]
                      ^ A[Idx(x,3) +: SP] ^ A[Idx(x,4) +: SP];
      C_rot[x*SP +: SP] = {2{C[x*SP +: SP]}} >> (SP-1);
    end
    for(x=0; x < 5; x=x+1) begin
      for(y=0; y < 5; y=y+1) begin
          D[Idx(x,y) +: SP] = A[Idx(x,y) +: SP]
              ^ C[(((x-1)+5) % 5)*SP +: SP] ^ C_rot[((x+1) % 5)*SP +: SP];
      end
    end

    Slices_DO = D;
  end

end else begin
  //-----------------------------------------------------------------------------
  // Iterative Theta step. Compute Theta on part of the state. The highest
  // slice's parity is temporarily saved and acts as input for the next
  // iteration. In the first iteration the first slice (z=0) is only partly
  // evaluated, since the last slice (z=W-1) is not available. It is finished in
  // the last iteration together with the last slice block.
  reg[4:0] TmpStorage_DP, TmpStorage_DN;

  always @(*) begin : THETA_ITERATIVE
    integer x, y;
    TmpStorage_DN = TmpStorage_DP;

    A = Slices_DI;
    for(x=0; x < 5; x=x+1) begin
      C[x*SP +: SP] = A[Idx(x,0) +: SP] ^ A[Idx(x,1) +: SP] ^ A[Idx(x,2) +: SP]
          ^ A[Idx(x,3) +: SP] ^ A[Idx(x,4) +: SP];
      C_rot[x*SP +: SP] = {2{C[x*SP +: SP]}} >> (SP-1);
      TmpStorage_DN[x] = C[x*SP + SP - 1]; // save highest slice parity for next block
    end
    for(x=0; x < 5; x=x+1) begin
      for(y=0; y < 5; y=y+1) begin
          D[Idx(x,y) +: SP] = A[Idx(x,y) +: SP] ^ C[(((x-1)+5) % 5)*SP +: SP]
              ^ {C_rot[((x+1)%5)*SP +: SP] >> 1, TmpStorage_DP[(x+1)%5]};
      end
    end

    Slices_DO = D;
  end

  always @(posedge Clk_CI or negedge Rst_RBI) begin
    if(~Rst_RBI) TmpStorage_DP <= {5{1'b0}};
    else if(RstSync_RI) TmpStorage_DP <= {5{1'b0}};
    else if(Enable_SI) TmpStorage_DP <= TmpStorage_DN;
  end

  always @(*) begin : LAST_SLICE
    integer x, y;
    for(x=0; x < 5; x=x+1) begin
      for(y=0; y < 5; y=y+1) begin
          SliceZ0_DO[5*x+y] = SliceZ0_DI[5*x+y] ^ TmpStorage_DN[(x+1)%5];
      end
    end
  end

end endgenerate

endmodule
