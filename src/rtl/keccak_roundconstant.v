`timescale 1ns / 1ps


module keccak_roundconstant #(
  parameter W = 16,
  parameter COUNTER_BITWIDTH = 4,
  parameter SLICES_PARALLEL = 1,
  parameter DOM_PIPELINE = 1,
  parameter SBOX_1CYCLE = 0
)
(
  input wire Clk_CI,
  input wire Rst_RBI,
  input wire[4:0] RoundNr_DI,
  input wire[COUNTER_BITWIDTH:0] SliceNr_DI,
  input wire[COUNTER_BITWIDTH:0] NextSliceNr_DI,
  input wire ResetRC_SI,
  input wire EnableRC_SI,
  output reg[SLICES_PARALLEL-1:0] RC_DO
  );

generate if(0 && SLICES_PARALLEL == 1 && W == 64) begin

  reg[7:0] RC_LFSR_DP, RC_LFSR_DN;
  reg RC_Zero_SP, RC_Zero_SN;
  always @(posedge Clk_CI or negedge Rst_RBI) begin
    if(~Rst_RBI) begin
      RC_LFSR_DP <= 8'h01;
      RC_Zero_SP <= 0;
    end else begin
      RC_LFSR_DP <= RC_LFSR_DN;
      RC_Zero_SP <= RC_Zero_SN;
    end
  end

  always @(*) begin : LFSR_UPDATE
    reg[7:0] r;
    reg[COUNTER_BITWIDTH:0] tmp;

    tmp = {COUNTER_BITWIDTH{1'b0}};
    if (DOM_PIPELINE) begin
      tmp = (SliceNr_DI == 0) ? 0 : (SliceNr_DI - 1);
    end else if (!SBOX_1CYCLE) begin
      tmp = NextSliceNr_DI >> 1;
    end else begin
      tmp = NextSliceNr_DI;
    end

    RC_Zero_SN = (tmp == 0) | (tmp == 1) | (tmp == 3) | (tmp == 7) | (tmp == 15)
                | (tmp == 31) | (tmp == 63);

    r = RC_LFSR_DP;
    RC_LFSR_DN = RC_LFSR_DP;

    if (ResetRC_SI) begin
      RC_LFSR_DN = 8'h01;
    end else if (EnableRC_SI) begin
      if (RC_Zero_SP) begin
        RC_LFSR_DN[7] = r[6] ^ r[5] ^ r[4] ^ r[0];
        RC_LFSR_DN[6:0] = r[7:1];
      end
    end

    RC_DO[0] = RC_LFSR_DP[0] & RC_Zero_SP;
  end

end else begin

  wire[24*64-1:0] RC = {
    64'h8000000080008008, 64'h0000000080000001, 64'h8000000000008080, 64'h8000000080008081,
    64'h800000008000000A, 64'h000000000000800A, 64'h8000000000000080, 64'h8000000000008002,
    64'h8000000000008003, 64'h8000000000008089, 64'h800000000000008B, 64'h000000008000808B,
    64'h000000008000000A, 64'h0000000080008009, 64'h0000000000000088, 64'h000000000000008A,
    64'h8000000000008009, 64'h8000000080008081, 64'h0000000080000001, 64'h000000000000808B,
    64'h8000000080008000, 64'h800000000000808A, 64'h0000000000008082, 64'h0000000000000001
  };

  always @(*) begin : SELECT_ROUND_CONSTANT
    integer i;
    reg[63:0] current_rc;
    i = SliceNr_DI;
    if(DOM_PIPELINE) begin
      i = SliceNr_DI;
    end else if(!SBOX_1CYCLE) begin
      i = i >> 1; // 2 cycles per iteration if DOM is applied and the inner domain register isn't clocked with the negative clock edge
    end
    current_rc = RC[RoundNr_DI*64 +: 64];
    RC_DO = EnableRC_SI ? current_rc[i*SLICES_PARALLEL +: SLICES_PARALLEL] : {SLICES_PARALLEL{1'b0}};
  end

end endgenerate

endmodule
