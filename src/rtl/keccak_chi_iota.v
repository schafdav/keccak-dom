`timescale 1ns/1ns

module keccak_chi_iota #(
  parameter SHARES = 2,
  parameter SLICES = 1,
  parameter CHI_DOUBLE_CLK = 1,
  parameter LESS_RAND = 0,
  parameter DOM_PIPELINE = 0
) (
  input wire Clk_CI,
  input wire Enable_SI,
  input wire Rst_RBI,
  input wire[SHARES*25*SLICES-1:0] Slices_DI,
  input wire[(SHARES*SHARES-SHARES)/2*25*SLICES-1:0] Z_DI,
  input wire[SLICES-1:0] IotaRC_DI,
  output reg[SHARES*25*SLICES-1:0] Slices_DO
);

localparam W = SLICES;
function integer Idx(input integer x, input integer y);
  Idx = (5*x+y)*W;
endfunction

function integer RowIdx(input integer share_nr, input integer idx_x, input integer idx_y);
  RowIdx = share_nr*25*SLICES + (5*idx_x + idx_y)*SLICES;
endfunction

//-----------------------------------------------------------------------------
// Generate unprotected Chi step
generate if(SHARES == 1) begin
  always @(*) begin : CHI_UNPROTECTED_COMB
    integer x0, x1, x2, y;
    reg[25*W-1:0] S, result;

    S = Slices_DI;
    result = {25*W{1'b0}};
    for(x0=0; x0 < 5; x0=x0+1) begin : SBOX_LOOP
      x1 = (x0 + 1) % 5;
      x2 = (x0 + 2) % 5;
      for(y=0; y < 5; y=y+1) begin
        //-------------------------------------------------------------
        // Chi step
        result[Idx(x0,y) +: W] = S[Idx(x0,y) +: W] ^ (~S[Idx(x1,y) +: W] & S[Idx(x2,y) +: W]);

        //-------------------------------------------------------------
        // Iota step
        if(x0 == 0 && y == 0) begin
            result[0 +: W] = result[0 +: W] ^ IotaRC_DI;
        end
      end
    end
    Slices_DO = result;
  end
end else if (1) begin
  //-----------------------------------------------------------------------------
  // Generate protected Chi step using individual S-Box instantiations
  genvar y, z;

  for(z = 0; z < SLICES; z=z+1) begin : GEN_SLICES
    for (y = 0; y < 5; y=y+1) begin : GEN_ROWS
      reg[SHARES*5-1 : 0] RowsInxD;
      wire[SHARES*5-1 : 0] RowsOutxD;
      reg[5*(SHARES*SHARES-SHARES)/2-1 : 0] RowsRandxD;

      always @(*) begin : ROW_SEL_IN
        integer i;
        for (i = 0; i < SHARES; i=i+1) begin
          RowsInxD[i*5 + 0] = Slices_DI[RowIdx(i,0,y) + z];
          RowsInxD[i*5 + 1] = Slices_DI[RowIdx(i,1,y) + z];
          RowsInxD[i*5 + 2] = Slices_DI[RowIdx(i,2,y) + z];
          RowsInxD[i*5 + 3] = Slices_DI[RowIdx(i,3,y) + z];
          RowsInxD[i*5 + 4] = Slices_DI[RowIdx(i,4,y) + z];
        end
        RowsRandxD = Z_DI[25*z + 5*y +: 5*(SHARES*SHARES - SHARES)/2];
      end

      always @(*) begin : ROW_SEL_OUT
        integer i;
        for (i = 0; i < SHARES; i=i+1) begin
          Slices_DO[RowIdx(i,0,y) + z] = RowsOutxD[i*5 + 0];
          Slices_DO[RowIdx(i,1,y) + z] = RowsOutxD[i*5 + 1];
          Slices_DO[RowIdx(i,2,y) + z] = RowsOutxD[i*5 + 2];
          Slices_DO[RowIdx(i,3,y) + z] = RowsOutxD[i*5 + 3];
          Slices_DO[RowIdx(i,4,y) + z] = RowsOutxD[i*5 + 4];
        end
      end

      if (y == 0) begin
        keccak_sbox
          #(.SHARES(SHARES)
          , .CHI_DOUBLE_CLK(CHI_DOUBLE_CLK)
          , .LESS_RAND(LESS_RAND)
          , .DOM_PIPELINE(DOM_PIPELINE)
          , .IOTA_XOR(1)
          ) sbox
          ( .Clk_CI(Clk_CI)
          , .Rst_RBI(Rst_RBI)
          , .Enable_SI(Enable_SI)
          , .IotaRC_DI(IotaRC_DI[z])
          , .Input_DI(RowsInxD)
          , .Z_DI(RowsRandxD)
          , .Output_DO(RowsOutxD)
          );
      end else begin
        keccak_sbox
          #(.SHARES(SHARES)
          , .CHI_DOUBLE_CLK(CHI_DOUBLE_CLK)
          , .LESS_RAND(LESS_RAND)
          , .DOM_PIPELINE(DOM_PIPELINE)
          , .IOTA_XOR(0)
          ) sbox
          ( .Clk_CI(Clk_CI)
          , .Rst_RBI(Rst_RBI)
          , .Enable_SI(Enable_SI)
          , .IotaRC_DI(1'b0)
          , .Input_DI(RowsInxD)
          , .Z_DI(RowsRandxD)
          , .Output_DO(RowsOutxD)
          );
      end

    end
  end
end
//-----------------------------------------------------------------------------
// Generate protected Chi step
else begin

  localparam NUM_FF = DOM_PIPELINE ? (SHARES*SHARES)*25*W
                                   : (SHARES*SHARES - SHARES)*25*W;
  reg[NUM_FF-1:0] FF_DN, FF_DP;
  if(CHI_DOUBLE_CLK) begin
    always @(negedge Clk_CI or negedge Rst_RBI) begin
      if(~Rst_RBI) FF_DP <= {NUM_FF{1'b0}};
      else if(Enable_SI) FF_DP <= FF_DN;
    end
  end else begin
    always @(posedge Clk_CI or negedge Rst_RBI) begin
      if(~Rst_RBI) FF_DP <= {NUM_FF{1'b0}};
      else if(Enable_SI) FF_DP <= FF_DN;
    end
  end

  always @(*) begin : SBOXES
    integer i, j, x0, x1, x2, y, ff_idx;
    reg[W-1:0] result;
    reg[25*W-1:0] S, T;
    reg[SHARES*25*W-1:0] SlicesxD;

    FF_DN = {NUM_FF{1'b0}};

    for(x0=0; x0 < 5; x0=x0+1) begin
      x1 = (x0 + 1) % 5;
      x2 = (x0 + 2) % 5;
      for(y=0; y < 5; y=y+1) begin
        for(i=0; i < SHARES; i=i+1) begin

          //---------------------------------------------------------
          // Chi step
          result = {W{1'b0}};
          S = Slices_DI[i*25*W +: 25*W];
          for(j=0; j < SHARES; j=j+1) begin
            T = Slices_DI[j*25*W +: 25*W];
            if(i==j) begin
              // inner domain term

              if(DOM_PIPELINE) begin
                ff_idx = i*SHARES+i;
                if(LESS_RAND && i >= SHARES-2) begin
                    FF_DN[ff_idx*25*W + Idx(x0,y) +: W] = (~S[Idx(x1,y) +: W] & S[Idx(x2,y) +: W]);
                end else begin
                    FF_DN[ff_idx*25*W + Idx(x0,y) +: W] = S[Idx(x0,y) +: W] ^ (~S[Idx(x1,y) +: W] & S[Idx(x2,y) +: W]);
                end
                result = result ^ FF_DP[ff_idx*25*W + Idx(x0,y) +: W];
              end else begin
                if(LESS_RAND && i >= SHARES-2) begin
                  // Don't XOR the A_xi part if that is done in the cross-domain term
                  result = result ^ (~S[Idx(x1,y) +: W] & S[Idx(x2,y) +: W]);
                end else begin
                  result = result ^ S[Idx(x0,y) +: W] ^ (~S[Idx(x1,y) +: W] & S[Idx(x2,y) +: W]);
                end
              end
            end else if(i < j) begin
              // cross domain term
              if(DOM_PIPELINE) begin
                ff_idx = i*SHARES + j;
              end else begin
                ff_idx = i*(SHARES-1) + j-1;
              end

              if(LESS_RAND && (i + j*(j-1)/2) == (SHARES*SHARES-SHARES)/2-1) begin
                FF_DN[ff_idx*25*W + Idx(x0,y) +: W]
                    = (S[Idx(x1,y) +: W] & T[Idx(x2,y) +: W]) ^ S[Idx(x0,y) +: W];
              end else begin
                FF_DN[ff_idx*25*W + Idx(x0,y) +: W]
                    = (S[Idx(x1,y) +: W] & T[Idx(x2,y) +: W])
                    ^ Z_DI[(i + j*(j-1)/2)*25*W + Idx(x0,y) +: W];
              end

              result = result ^ FF_DP[ff_idx*25*W + Idx(x0,y) +: W];

              //---------------------------------------------------------
              // Iota step
              if(i == 0 && x0 == 0 && y == 0 && (i + j*(j-1)/2)==0) begin
                FF_DN[ff_idx*25*W + Idx(x0,y) +: W] = IotaRC_DI ^ FF_DN[ff_idx*25*W + Idx(x0,y) +: W];
              end
            end
            else if(i > j) begin
              // cross domain term
              if(DOM_PIPELINE) begin
                ff_idx = i*SHARES + j;
              end else begin
                ff_idx = i*(SHARES-1) + j;
              end

              if(LESS_RAND && (j + i*(i-1)/2) == (SHARES*SHARES-SHARES)/2-1) begin
                FF_DN[ff_idx*25*W + Idx(x0,y) +: W]
                  = (S[Idx(x1,y) +: W] & T[Idx(x2,y) +: W]) ^ S[Idx(x0,y) +: W];
              end else begin
                FF_DN[ff_idx*25*W + Idx(x0,y) +: W]
                  = (S[Idx(x1,y) +: W] & T[Idx(x2,y) +: W])
                  ^ Z_DI[(j + i*(i-1)/2)*25*W + Idx(x0,y) +: W];
              end

              result = result ^ FF_DP[ff_idx*25*W + Idx(x0,y) +: W];
            end
          end
          Slices_DO[i*25*W + Idx(x0,y) +: W] = result;

        end // for i
      end // for y
    end // for x0

  end // always

end endgenerate

endmodule
