`timescale 1ns/1ns

module keccak_top #(
    // BASIC OPTIONS
    parameter RATE = 128, // The rate of the sponge construction
    parameter W = 16,      // The width of one Keccak register, aka lane length
    parameter SHARES = 2,  // Number of shares to use (SHARES-1 == protection order).

    //TODO Would be nice to have a coarse configuration setting in case we don't need full configurability
    // 0 = Minimum area. Detailed description goes here
    // 1 = Almost minimum area. Detailed description goes here
    // ...
    //parameter CONFIGURATION = 0,


    // ADVANCED OPTIONS

    // Number of lanes that are absorbed in one cycle.
    // You probably want to set ABSORB_ITERATIVE=0 if this is not RATE/W,
    // otherwise absorbtion will take a significant amount of time.
    parameter ABSORB_LANES = RATE/W,

    // Note: If all of the following *_ITERATIVE options are 0, all steps
    //       are done in one cycle. Consequently the whole computation takes
    //       12+2*ld(W) (== #rounds) cycles

    // 0 = State reset is done asynchronously
    // 1 = State reset takes W cycles, but the more expensive Clear FF are
    //     not used
    parameter RESET_ITERATIVE = 0,

    // 0 = Absorb in one cycle. (Automatically true if W==SLICES_PARALLEL)
    // 1 = Absorb concurrently with Theta step if W!=SLICES_PARALLEL.
    // ABSORB_ITERATIVE implies THETA_ITERATIVE, thus THETA_ITERATIVE is
    // ignored if (ABSORB_ITERATIVE == 1)
    parameter ABSORB_ITERATIVE = 1,

    // 0 = Theta executed in 1 cycle
    // 1 = Theta takes W/SLICES_PARALLEL cycles
    // ABSORB_ITERATIVE implies THETA_ITERATIVE, thus THETA_ITERATIVE is
    // ignored if (ABSORB_ITERATIVE == 1)
    parameter THETA_ITERATIVE = 1,

    //TODO rename
    // 0 = Rho+Pi executed in 1 cycle
    // 1 = Rho is done in W cycles, Pi takes W/SLICES_PARALLEL cycles
    parameter RHO_PI_ITERATIVE = 0,

    // 0 = Chi+Iota executed in 1 cycle
    // 1 = Chi+Iota takes W/SLICES_PARALLEL cycles if unprotected, and
    //     2*W/SLICES_PARALLEL cycles if DOM is used (pipelining and double
    //     clocking decrease this to W/SLICES_PARALLEL+1 and W/SLICES_PARALLEL
    //     respectively)
    parameter CHI_IOTA_ITERATIVE = 1,

    // The iterative steps operate on SLICES_PARALLEL slices.
    // Each iterative step thus takes W/SLICES_PARALLEL cycles.
    // Note that an iterative Chi step drastically reduces the required
    // randomness of a masked configuration (see further documentation for
    // details).
    parameter SLICES_PARALLEL = 1,

    // The Chi step takes 2*ABSORB_SLICES cycles in DOM. Clock the inner domain
    // register with the negative edge to overcome this. This should hardly
    // influence the critical path, but increase throughput.
    parameter CHI_DOUBLE_CLK = 0,

    parameter LESS_RAND = 1,

    // Controls the insertion of the inner-domain FF when using more than 1 share
    // 0 = Only the cross-domain-FF are inserted
    // 1 = Inner-domain FF are inserted and used as pipeline stage if the
    //     configuration allows it
    parameter DOM_PIPELINE = 1,

    // LOCAL PARAMETERS - DO NOT MODIFY
    parameter ABSORB_SLICES = ABSORB_ITERATIVE ? SLICES_PARALLEL : W,
    parameter THETA_SLICES = THETA_ITERATIVE ? SLICES_PARALLEL : ABSORB_SLICES,
    parameter CHI_SLICES = CHI_IOTA_ITERATIVE ? SLICES_PARALLEL : W,
    parameter CONNECT_ABSORB_CHI = (ABSORB_ITERATIVE && CHI_IOTA_ITERATIVE && RATE/W == ABSORB_LANES) ? 1 : 0,
    parameter DATAOUT_SIZE = (CONNECT_ABSORB_CHI) ? 25*SLICES_PARALLEL : RATE
)(
    input  wire Clk_CI,
    input  wire Rst_RBI,

    input  wire RandomnessAvailable_SI,
    input  wire StartAbsorb_SI,
    input  wire StartSqueeze_SI,
    output wire Ready_SO,
    input  wire[SHARES*(ABSORB_LANES*ABSORB_SLICES)-1:0] AbsorbSlices_DI,
    //TODO Z_DI length can be adjusted to LESS_RAND (should be no functional difference, since upper bits are just unused at the moment in that case)
    input  wire[(SHARES*SHARES-SHARES)/2 * 25 * CHI_SLICES - 1:0] Z_DI,
    output reg[SHARES*DATAOUT_SIZE-1:0] Data_DO
);

localparam STATE_SIZE = 5*5*W;
localparam THETA_SLICES_SIZE = 5*5*THETA_SLICES;
localparam CHI_SLICES_SIZE = 5*5*CHI_SLICES;
localparam PI_SLICES = SLICES_PARALLEL; //TODO hmm
localparam PI_SLICES_SIZE = 5*5*PI_SLICES;
localparam ABSORB_SLICES_SIZE = ABSORB_SLICES * ABSORB_LANES;

function integer getLaneNr(input integer x_coord, input integer y_coord);
    getLaneNr = 5*x_coord + y_coord;
endfunction

function integer getXCoord(input integer lane_nr);
    getXCoord = lane_nr / 5;
endfunction

function integer getYCoord(input integer lane_nr);
    getYCoord = lane_nr % 5;
endfunction

function integer Idx(input integer s,
                     input integer x,
                     input integer y,
                     input integer len);
    Idx = s*25*len + getLaneNr(x,y)*len;
endfunction

function integer StateIdx(input integer s, input integer x, input integer y);
    StateIdx = s*STATE_SIZE + getLaneNr(x,y)*W;
endfunction

`define QUEUE(target_state, slices, slice_len, shift) \
    for(i=0; i < SHARES; i=i+1) begin \
        for(x=0; x < 5; x=x+1) begin \
            for(y=0; y < 5; y=y+1) begin \
                target_state[StateIdx(i,x,y) +: W] = { \
                    slices[Idx(i,x,y,slice_len) +: shift], \
                    StatexD[StateIdx(i,x,y) +: W] } >> shift; \
            end \
        end \
    end

`define CON_SLICES(dst, dst_len, src, src_len, start_slice, amount) \
    for(i=0; i < SHARES; i=i+1) begin \
        for(x=0; x < 5; x=x+1) begin \
            for(y=0; y < 5; y=y+1) begin \
                dst[Idx(i,x,y,dst_len) +: amount] \
                    = src[Idx(i,x,y,src_len) + start_slice +: amount]; \
            end \
        end \
    end

`define CON_ABSORB_XOR(dst, dst_len, src, src_len, amount) \
    for(i=0; i < SHARES; i=i+1) begin \
        for(x=0; x < 5; x=x+1) begin \
            for(y=0; y < 5; y=y+1) begin \
                if(x+5*y < RATE/W) begin \
                    dst[Idx(i,x,y, dst_len) +: amount] \
                        = AbsorbSlices_DI[i*(ABSORB_LANES*ABSORB_SLICES) + ((x+5*y)%ABSORB_LANES)*ABSORB_SLICES +: ABSORB_SLICES] \
                        ^ src[Idx(i,x,y,src_len) +: amount]; \
                end \
            end \
        end \
    end

wire[SHARES*STATE_SIZE-1:0] State_D;
reg[SHARES*5*5*THETA_SLICES-1:0] SlicesToTheta_D;
wire[SHARES*5*5*THETA_SLICES-1:0] SlicesFromTheta_D;

reg[SHARES*5*5-1:0] SliceZ0ToTheta_D;
wire[SHARES*5*5-1:0] SliceZ0FromTheta_D;

wire[SHARES*5*5*SLICES_PARALLEL-1:0] SlicesFromPi_D;
reg[SHARES*5*5*SLICES_PARALLEL-1:0] SlicesToPi_D;

wire[SHARES*STATE_SIZE-1:0] StateFromRhoPi_D;
reg[SHARES*STATE_SIZE-1:0] StateToRhoPi_D;

reg[SHARES*5*5*CHI_SLICES-1:0] SlicesToChi_D;
wire[SHARES*5*5*CHI_SLICES-1:0] SlicesFromChi_D;

wire[24:0] ctrl_enable_lane;
wire ctrl_enable_absorb;
wire ctrl_enable_lambda;
wire ctrl_enable_theta;
wire ctrl_enable_rhopi;
 // verilator lint_off UNUSED
wire ctrl_enable_chi_iota;
// verilator lint_on UNUSED
wire ctrl_theta_last;
wire ctrl_enable_absorb_theta;
wire ctrl_enable_DOM_ff;
wire ctrl_reset_state;

generate
  genvar s;
  for(s = 0; s < SHARES; s=s+1) begin : gen_linear_steps

    //---------------------------------------------------------------------
    // States

    keccak_state #(
      .RATE               (RATE),
      .W                  (W),
      .RESET_ITERATIVE    (RESET_ITERATIVE),
      .ABSORB_ITERATIVE   (ABSORB_ITERATIVE),
      .THETA_ITERATIVE    (THETA_ITERATIVE),
      .RHO_PI_ITERATIVE   (RHO_PI_ITERATIVE),
      .CHI_IOTA_ITERATIVE (CHI_IOTA_ITERATIVE),
      .CONNECT_ABSORB_CHI (CONNECT_ABSORB_CHI),
      .SLICES_PARALLEL    (SLICES_PARALLEL),
      .THETA_SLICES       (THETA_SLICES),
      .CHI_SLICES         (CHI_SLICES),
      .ABSORB_LANES       (ABSORB_LANES),
      .ABSORB_SLICES      (ABSORB_SLICES)
      ) SHARE (
      .Clk_CI              (Clk_CI),
      .Rst_RBI             (Rst_RBI),
      .EnableLane_SI       (ctrl_enable_lane),
      .ctrl_reset_state    (ctrl_reset_state),
      .ctrl_enable_rhopi   (ctrl_enable_rhopi),
      .ctrl_enable_theta   (ctrl_enable_theta),
      .ctrl_theta_last     (ctrl_theta_last),
      .ctrl_enable_lambda  (ctrl_enable_lambda),
      .ctrl_enable_absorb  (ctrl_enable_absorb),
      .AbsorbSlices_DI     (AbsorbSlices_DI[s*(ABSORB_LANES*ABSORB_SLICES) +: ( ABSORB_LANES*ABSORB_SLICES)]),
      .SliceZ0FromTheta_DI (SliceZ0FromTheta_D[s*25 +: 25]),
      .SlicesFromTheta_DI  (SlicesFromTheta_D[s*THETA_SLICES_SIZE +: THETA_SLICES_SIZE]),
      .StateFromRhoPi_DI   (StateFromRhoPi_D[s*STATE_SIZE +: STATE_SIZE]),
      .SlicesFromChi_DI    (SlicesFromChi_D[s*CHI_SLICES_SIZE +: CHI_SLICES_SIZE]),
      .State_DO            (State_D[s*STATE_SIZE +: STATE_SIZE])
    );

    //---------------------------------------------------------------------
    // Theta

    keccak_theta #(.W(W), .SLICES_PARALLEL(THETA_SLICES)) THETA(
      .Clk_CI     (Clk_CI),
      .Rst_RBI    (Rst_RBI),
      .RstSync_RI (ctrl_theta_last),
      .Enable_SI  (ctrl_enable_theta),
      .Slices_DI  (SlicesToTheta_D[s*THETA_SLICES_SIZE +: THETA_SLICES_SIZE]),
      .SliceZ0_DI (SliceZ0ToTheta_D[s*25 +: 25]),
      .Slices_DO  (SlicesFromTheta_D[s*THETA_SLICES_SIZE +: THETA_SLICES_SIZE]),
      .SliceZ0_DO (SliceZ0FromTheta_D[s*25 +: 25])
    );

    //---------------------------------------------------------------------
    // Rho + Pi

    if(RHO_PI_ITERATIVE) begin
      keccak_pi #(
        .SLICES_PARALLEL (PI_SLICES)
        ) PI (
        .Slices_DI (SlicesToPi_D[s*PI_SLICES_SIZE +: PI_SLICES_SIZE]),
        .Slices_DO (SlicesFromPi_D[s*PI_SLICES_SIZE +: PI_SLICES_SIZE])
      );
    end else begin
      keccak_rhopi #(
        .W (W)
        ) RHOPI (
        .State_DI (StateToRhoPi_D[s*STATE_SIZE +: STATE_SIZE]),
        .State_DO (StateFromRhoPi_D[s*STATE_SIZE +: STATE_SIZE])
      );
    end

  end
endgenerate

//-----------------------------------------------------------------------------
// Chi + Iota

wire[CHI_SLICES-1:0] IotaRC_D;
keccak_chi_iota #(
  .SHARES         (SHARES),
  .SLICES         (CHI_SLICES),
  .CHI_DOUBLE_CLK (CHI_DOUBLE_CLK),
  .LESS_RAND      (LESS_RAND),
  .DOM_PIPELINE   (DOM_PIPELINE)
  ) CHI (
  .Clk_CI    (Clk_CI),
  .Enable_SI (ctrl_enable_DOM_ff),
  .Rst_RBI   (Rst_RBI),
  .Slices_DI (SlicesToChi_D),
  .Z_DI      (Z_DI),
  .IotaRC_DI (IotaRC_D),
  .Slices_DO (SlicesFromChi_D)
);

//-----------------------------------------------------------------------------
// Connection of the steps in different configurations

generate
  if(CONNECT_ABSORB_CHI) begin
    // Outputs are slices from Chi. User has to store them and put them
    // back together
    always @(*) begin : OUTPUT_CONNECT
      Data_DO = SlicesFromChi_D;
    end
  end else begin
    // Output is part of the state
    always @(*) begin : OUTPUT_CONNECT
      integer i, x, y;
      for(i=0; i < SHARES; i=i+1)
        for(x=0; x < 5; x=x+1)
          for(y=0; y < 5; y=y+1)
            if(x+5*y < RATE/W) // First Rate/W lanes
              Data_DO[i*RATE + (x+5*y)*W +: W] = State_D[StateIdx(i,x,y) +: W];
    end
  end
endgenerate


//`CON_SLICES(dst, dst_len, src, src_len, start_slice, amount)
generate
  if (THETA_ITERATIVE || ABSORB_ITERATIVE) begin
    always @(*) begin : THETA_CONNECT
      integer i, x, y;
      `CON_SLICES(SliceZ0ToTheta_D, 1, State_D, W, THETA_SLICES, 1)
    end
  end else begin
    always @(*) begin : THETA_CONNECT
      SliceZ0ToTheta_D = {SHARES*25{1'b0}};
    end
  end
endgenerate

generate
  if (CONNECT_ABSORB_CHI) begin
    // ABSORB_SLICES == THETA_SLICES == CHI_SLICES
    always @(*) begin : ABSORB_CONNECT
      integer i, x, y;
      `CON_SLICES(SlicesToTheta_D, THETA_SLICES, SlicesFromChi_D, CHI_SLICES, 0, THETA_SLICES)
      if(ctrl_enable_absorb_theta) begin
        `CON_ABSORB_XOR(SlicesToTheta_D, THETA_SLICES, SlicesFromChi_D, CHI_SLICES, THETA_SLICES)
      end
    end
  end else if (ABSORB_ITERATIVE) begin
    // Absorbtion and Theta step concurrent in slice based absorbtion
    always @(*) begin : ABSORB_CONNECT
      integer i, x, y;
      `CON_SLICES(SlicesToTheta_D, THETA_SLICES, State_D, W, 0, THETA_SLICES)
      if(ctrl_enable_absorb_theta) begin
        `CON_ABSORB_XOR(SlicesToTheta_D, THETA_SLICES, State_D, W, THETA_SLICES)
      end
    end
  end else begin
    always @(*) begin : ABSORB_CONNECT
      integer i, x, y;
      `CON_SLICES(SlicesToTheta_D, THETA_SLICES, State_D, W, 0, THETA_SLICES)
    end
  end
endgenerate

//`CON_SLICES(dst, dst_len, src, src_len, start_slice, amount)
generate
  if(RHO_PI_ITERATIVE) begin
    always @(*) begin : RHO_PI_CONNECT
      integer i, x, y;
      `CON_SLICES(SlicesToPi_D, PI_SLICES, State_D, W, 0, PI_SLICES)
    end
  end else if(!RHO_PI_ITERATIVE && !THETA_ITERATIVE && !ABSORB_ITERATIVE) begin
    // Linear steps not iterative -> chain Theta+Rho+Pi steps together
    always @(*) begin : RHO_PI_CONNECT
      integer i, x, y;
      `CON_SLICES(StateToRhoPi_D, W, SlicesFromTheta_D, W, 0, W)
    end
  end else begin
    always @(*) begin : RHO_PI_CONNECT
      StateToRhoPi_D = State_D;
    end
  end
endgenerate


//`CON_SLICES(dst, dst_len, src, src_len, start_slice, amount)
generate
  if(CHI_IOTA_ITERATIVE && RHO_PI_ITERATIVE) begin
    always @(*) begin : CHI_CONNECT
      integer i, x, y;
      `CON_SLICES(SlicesToChi_D, CHI_SLICES, SlicesFromPi_D, PI_SLICES, 0, CHI_SLICES)
    end
  end else if (CHI_IOTA_ITERATIVE && !RHO_PI_ITERATIVE || (SHARES > 1)) begin
    always @(*) begin : CHI_CONNECT
      integer i, x, y;
      `CON_SLICES(SlicesToChi_D, CHI_SLICES, State_D, W, 0, CHI_SLICES)
    end
  end else begin
    always @(*) begin : CHI_CONNECT
      integer i, x, y;
      `CON_SLICES(SlicesToChi_D, CHI_SLICES, StateFromRhoPi_D, W, 0, CHI_SLICES)
    end
  end
endgenerate

//-----------------------------------------------------------------------------
// Control path

keccak_control #(
  .RATE               (RATE),
  .W                  (W),
  .SHARES             (SHARES),
  .ABSORB_LANES       (ABSORB_LANES),
  .RESET_ITERATIVE    (RESET_ITERATIVE),
  .ABSORB_ITERATIVE   (ABSORB_ITERATIVE),
  .THETA_ITERATIVE    (THETA_ITERATIVE),
  .RHO_PI_ITERATIVE   (RHO_PI_ITERATIVE),
  .CHI_IOTA_ITERATIVE (CHI_IOTA_ITERATIVE),
  .SLICES_PARALLEL    (SLICES_PARALLEL),
  .ABSORB_SLICES      (ABSORB_SLICES),
  .THETA_SLICES       (THETA_SLICES),
  .CHI_SLICES         (CHI_SLICES),
  .CHI_DOUBLE_CLK     (CHI_DOUBLE_CLK),
  .CONNECT_ABSORB_CHI (CONNECT_ABSORB_CHI),
  .DOM_PIPELINE       (DOM_PIPELINE)
  ) KECCAK_CONTROL (
  .Clk_CI                 (Clk_CI),
  .Rst_RBI                (Rst_RBI),
  .StartAbsorb_SI         (StartAbsorb_SI),
  .StartSqueeze_SI        (StartSqueeze_SI),
  .RandomnessAvailable_SI (RandomnessAvailable_SI),
  .Ready_SO               (Ready_SO),
  .IotaRC_DO              (IotaRC_D),
  .StateCtrl_SO           ({ctrl_enable_lane,
                            ctrl_enable_absorb,
                            ctrl_enable_lambda,
                            ctrl_enable_theta,
                            ctrl_enable_rhopi,
                            ctrl_enable_chi_iota,
                            ctrl_theta_last,
                            ctrl_enable_absorb_theta,
                            ctrl_enable_DOM_ff,
                            ctrl_reset_state } )
);


`ifndef SYNTHESIS
//-----------------------------------------------------------------------------
// Sanity checks

`define assert(_cond, _msg) if (!(_cond)) begin $error(_msg); $finish(1); end

initial begin : SANITY_CHECKS
  // Some parameter sanity checks
  `assert((RATE % W == 0) && (RATE <= 25*W),
    "The sponge RATE must be a multiple of the lane length.");
  `assert(SLICES_PARALLEL <= W,
    "Can't process more than the full state in a single cycle.");
  `assert(W == 1 || W == 2 || W == 4 || W == 8 || W == 16 || W == 32 || W == 64,
    "Lane length 'W' must be a power of two and smaller or equal to 64.");
  `assert( !((W == SLICES_PARALLEL) && (ABSORB_ITERATIVE || THETA_ITERATIVE || RHO_PI_ITERATIVE || CHI_IOTA_ITERATIVE)) ,
    "Parameters are chosen such that the whole state gets processed in one cycle. However at least on step should be performed iteratively?!");
  `assert( !((W != SLICES_PARALLEL) && !(ABSORB_ITERATIVE || THETA_ITERATIVE || RHO_PI_ITERATIVE || CHI_IOTA_ITERATIVE)) ,
    "No iterative step, but also not processing the full state in one cycle?!");
  if( (W == SLICES_PARALLEL) && SHARES > 1 ) begin
    $warning("Huge randomness requirement in CHI step. Are you sure you want this?");
  end
  `assert( (ABSORB_LANES >= 1) && ABSORB_LANES <= RATE/W && (RATE/W % ABSORB_LANES == 0) ,
    "The ABSORB_LANES parameter should be between 1 and RATE/W. Additionally it must divide RATE/W without reminder");
  `assert( !(RHO_PI_ITERATIVE && !CHI_IOTA_ITERATIVE) ,
    "Not useful because RHO and PI will take W cycles each. Set CHI_IOTA_ITERATIVE so that the Pi step is done concurrently with CHI.");
  `assert( !(RHO_PI_ITERATIVE && SLICES_PARALLEL != 1),
    "The iterative Rho step is done by shifting the lanes. Thus we need W cycles and not W over SLICES_PARALLEL");
  `assert( !(CHI_DOUBLE_CLK && DOM_PIPELINE) ,
    "CHI_DOUBLE_CLK and DOM_PIPELINE cannot both be true.");
end
`endif

endmodule
