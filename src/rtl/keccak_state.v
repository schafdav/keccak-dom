`timescale 1ns/1ns

module keccak_state #(
  parameter RATE = 128,
  parameter W = 16,

  parameter RESET_ITERATIVE = 1,
  parameter ABSORB_ITERATIVE = 1,
  parameter THETA_ITERATIVE = 1,
  parameter RHO_PI_ITERATIVE = 0,
  parameter CHI_IOTA_ITERATIVE = 1,

  parameter CONNECT_ABSORB_CHI = 1,

  parameter SLICES_PARALLEL = 1,
  parameter THETA_SLICES = 1,
  parameter CHI_SLICES = 1,
  parameter ABSORB_LANES = RATE/W,
  parameter ABSORB_SLICES = W
)(
  input  wire Clk_CI,
  input  wire Rst_RBI,
  input  wire[24:0] EnableLane_SI,
  input  wire ctrl_reset_state,
  input  wire ctrl_enable_rhopi,
  input  wire ctrl_enable_theta,
  input  wire ctrl_theta_last,
  input  wire ctrl_enable_lambda,
  input  wire ctrl_enable_absorb,
  input  wire[(ABSORB_LANES*ABSORB_SLICES)-1:0] AbsorbSlices_DI,
  input  wire[24:0] SliceZ0FromTheta_DI,
  input  wire[25*THETA_SLICES-1:0] SlicesFromTheta_DI,
  input  wire[25*W-1:0] StateFromRhoPi_DI,
  input  wire[25*CHI_SLICES-1:0] SlicesFromChi_DI,
  output wire[25*W-1:0] State_DO
);

localparam STATE_SIZE = 5*5*W;

function integer getLaneNr(input integer x_coord, input integer y_coord);
  getLaneNr = 5*x_coord + y_coord;
endfunction

function integer Idx(input integer x,
                     input integer y,
                     input integer len);
  Idx = getLaneNr(x,y)*len;
endfunction

`define QUEUE2(target_state, slices, slice_len, shift) \
  for(x=0; x < 5; x=x+1) begin \
    for(y=0; y < 5; y=y+1) begin \
      target_state[Idx(x,y,W) +: W] = { \
          slices[Idx(x,y,slice_len) +: shift], \
          State_DP[Idx(x,y,W) + shift +: W-shift] }; \
    end \
  end

`define CON_SLICES2(dst, dst_len, src, src_len, start_slice, amount) \
  for(x=0; x < 5; x=x+1) begin \
    for(y=0; y < 5; y=y+1) begin \
      dst[Idx(x,y,dst_len) +: amount] \
          = src[Idx(x,y,src_len) + start_slice +: amount]; \
    end \
  end

`define CON_ABSORB_XOR2(dst, dst_len, src, src_len, amount) \
  for(x=0; x < 5; x=x+1) begin \
    for(y=0; y < 5; y=y+1) begin \
      if(x+5*y < RATE/W) begin \
        dst[Idx(x,y, dst_len) +: amount] \
            = AbsorbSlices_DI[((x+5*y)%ABSORB_LANES)*ABSORB_SLICES +: ABSORB_SLICES] \
            ^ src[Idx(x,y,src_len) +: amount]; \
      end \
    end \
  end

reg[STATE_SIZE-1:0] SlicesToState_D;
reg[STATE_SIZE-1:0] State_DP;

always @(*) begin : STATE_CONNECT
  integer x, y;
  reg[STATE_SIZE-1:0] tmp;
  //SlicesToState_D = State_DP;

  if(CONNECT_ABSORB_CHI) begin
    if(RESET_ITERATIVE && ctrl_reset_state) begin
      tmp = {STATE_SIZE{1'b0}};
      `QUEUE2(SlicesToState_D, tmp, W, THETA_SLICES)
    end else if(ctrl_enable_rhopi) begin
      if (RHO_PI_ITERATIVE) begin
        `QUEUE2(SlicesToState_D, State_DP, W, 1)
      end else begin
        SlicesToState_D = StateFromRhoPi_DI;
      end
    end else begin
      `QUEUE2(SlicesToState_D, SlicesFromTheta_DI, THETA_SLICES, THETA_SLICES)
      if(ctrl_theta_last) begin
        //`CON_SLICES2(dst, dst_len, src, src_len, start_slice, amount)
        `CON_SLICES2(SlicesToState_D, W, SliceZ0FromTheta_DI, 1, 0, 1)
      end
    end
  end else begin
    case(1'b1)
    RESET_ITERATIVE && ctrl_reset_state: begin
      tmp = {STATE_SIZE{1'b0}};
      `QUEUE2(SlicesToState_D, tmp, W, THETA_SLICES)
    end

    ctrl_enable_absorb: begin
      if (!ABSORB_ITERATIVE) begin
        `CON_ABSORB_XOR2(SlicesToState_D, W, State_DP, W, W)
      end else if (W!=ABSORB_SLICES && ABSORB_LANES < RATE/W) begin
        `CON_ABSORB_XOR2(tmp, W, State_DP, W, W)
        `QUEUE2(SlicesToState_D, tmp, W, ABSORB_SLICES)
      end
    end

    ctrl_enable_lambda: begin
      SlicesToState_D = StateFromRhoPi_DI;
    end

    ctrl_enable_rhopi: begin
      if (RHO_PI_ITERATIVE) begin
        `QUEUE2(SlicesToState_D, State_DP, W, 1)
      end else begin
        SlicesToState_D = StateFromRhoPi_DI;
      end
    end

    ctrl_enable_theta: begin
      `QUEUE2(SlicesToState_D, SlicesFromTheta_DI, THETA_SLICES, THETA_SLICES)
      if ((ABSORB_ITERATIVE || THETA_ITERATIVE) && ctrl_theta_last) begin
        //`CON_SLICES2(dst, dst_len, src, src_len, start_slice, amount)
        `CON_SLICES2(SlicesToState_D, W, SliceZ0FromTheta_DI, 1, 0, 1)
      end
    end

    default: begin
      // Use Chi/Iota as the default state, since the enable signals
      // handle the state update anyway. Decreases gate count, because
      // the synthesizer can't infer that SlicesToState_D = State_DP
      // is already done by these enable signals.
      `QUEUE2(SlicesToState_D, SlicesFromChi_DI, CHI_SLICES, CHI_SLICES)
    end
    endcase
  end

end



generate
  if (!RESET_ITERATIVE) begin
    always @(posedge Clk_CI or negedge Rst_RBI) begin : STATE
      integer regcnt;
      if (~Rst_RBI) begin
        State_DP <= {STATE_SIZE{1'b0}};
      end else begin
        for(regcnt=0; regcnt < 25; regcnt=regcnt+1) begin
          if (EnableLane_SI[regcnt]) begin
            State_DP[regcnt*W +: W] <= SlicesToState_D[regcnt*W +: W];
          end
        end
      end
    end
  end else begin
    always @(posedge Clk_CI) begin : STATE
      integer regcnt;
      for(regcnt=0; regcnt < 25; regcnt=regcnt+1) begin
        if (EnableLane_SI[regcnt]) begin
          State_DP[regcnt*W +: W] <= SlicesToState_D[regcnt*W +: W];
        end
      end
    end
  end
endgenerate

assign State_DO = State_DP;

endmodule
