from abc import ABCMeta, abstractmethod

class KeccakInterface():
    __metaclass__ = ABCMeta

    @abstractmethod
    def absorb(self, block):
        pass

    @abstractmethod
    def squeeze(self):
        pass

    @abstractmethod
    def theta(self):
        pass

    @abstractmethod
    def rho_pi(self):
        pass

    @abstractmethod
    def chi(self):
        pass

    @abstractmethod
    def iota(self):
        pass
