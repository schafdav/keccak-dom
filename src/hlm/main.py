from keccak import Keccak
import random, argparse

def debug(msg, dbg_level):
    if dbg_level > 0:
        print(msg)

class Test:
    def __init__(self, message, hash = None):
        self.message = message
        self.hash = hash

    def run(self, keccak, selftest=False, logfile=None):
        keccak.reset()
        keccak.absorb(self.message)

        if selftest:
            # Testing the HLM itself
            assert self.hash

            num_rate_chars = keccak.rate//8*2
            result = ""
            while len(result) < len(self.hash):
                result += keccak.squeeze()[:num_rate_chars].upper()
            assert result.startswith(self.hash.upper()), "\nexpected '{}'\nreceived '{}'".format(self.hash, result)

        if logfile:
            with open(logfile, 'a') as f:
                keccak.write_log(f)

def test_hlm(args):
    """Test the HLM itself"""
    random.seed(21) # yields 42 for random.choice(range(256)) the first time :-)

    debug("========== Testing Keccak[1088,512] ==========", args.verbose)
    r = 1088 ; c = 512
    keccak_impls = [Keccak(rate=r, capacity=c, num_shares=x, dbg_level=args.verbose-1) for x in range(1, args.shares+1)]
    tests = [
        # Empty message
        Test(b"", hash='C5D2460186F7233C927E7DB2DCC703C0E500B653CA82273B7BFAD8045D85A470'),
        # Short message
        Test(b"hi", hash='7624778DEDC75F8B322B9FA1632A610D40B85E106C7D9BF0E743A9CE291B9C6F'),
        # Exactly one block minus 1 byte long (only 1 block absorbed)
        Test(b"0"*(r//8-1), hash='56837857BFC58F5CFF3F32390FE636BA440553E60C141597A85499AD632A7A0A'),
        # Exactly one block long (2 blocks absorbed)
        Test(b"0"*(r//8), hash='FB55DCBD088DA4747E7FED1CF2FD8A211525D566CBA122EA8C0940C71B8B67C8'),
        # Long message (multiple absorbtions)
        Test(b"abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ 01234567890 "*10, hash='EA3F572484FC29FE9776391C4364E2999E14FD7572BB9E288FA59A2F981ED536'),
    ]
    for i, t in enumerate(tests):
        debug("Run test nr. {}".format(i), args.verbose)
        for k in keccak_impls:
            t.run(k, selftest=True)

    debug("========== Testing Keccak[1152,448] ==========", args.verbose)
    r = 1152 ; c = 448
    keccak_impls = [Keccak(rate=r, capacity=c, num_shares=x, dbg_level=args.verbose-1) for x in range(1, args.shares+1)]
    tests = [
        # Empty message
        Test(b"", hash='f71837502ba8e10837bdd8d365adb85591895602fc552b48b7390abd'),
        # Short message
        Test(b"hi", hash='a8c2b88be17bdda827fc5c7ec3f80e2579d4ba96f6c5dbd850f23edf'),
        # Exactly one block minus 1 byte long (only 1 block absorbed)
        Test(b"0"*(r//8-1), hash='2e0481d4aaf3777adb4069f0488138d1284ae73975336361d08673d3'),
        # Exactly one block long (2 blocks absorbed)
        Test(b"0"*(r//8), hash='06b25f7cadf3b750114a7bfc608079734a137a8dfd187ff2595b97cc'),
        # Long message (multiple absorbtions)
        Test(b"abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ 01234567890 "*10, hash='6b0d84e78dc905920213d02514c57d67f697d458dae0f43bb7fd52b7'),
    ]
    for i, t in enumerate(tests):
        debug("Run test nr. {}".format(i), args.verbose)
        for k in keccak_impls:
            t.run(k, selftest=True)

    debug("========== Testing Keccak[576,1024] ==========", args.verbose)
    r = 576 ; c = 1024
    keccak_impls = [Keccak(rate=r, capacity=c, num_shares=x, dbg_level=args.verbose-1) for x in range(1, args.shares+1)]
    tests = [
        # Empty message
        Test(b"", hash='0eab42de4c3ceb9235fc91acffe746b29c29a8c366b7c60e4e67c466f36a4304c00fa9caf9d87976ba469bcbe06713b435f091ef2769fb160cdab33d3670680e'),
        # Short message
        Test(b"hi", hash='02a43a9688ca677d9d145232c48be9b7b99f6745bb70bafb6d4f30d92d3277f3d5593eab808539da34eade7acd354f0e1a5146da598241771946f26df743f236'),
        # Exactly one block minus 1 byte long (only 1 block absorbed)
        Test(b"0"*(r//8-1), hash='40b4f737aa2f8876b74cd8052ee2b366c55f906e836dbcca3b47313f8ef40b86f8c95cb4182075d2ce83f2a5cae79ae74da559b9bd074219365207704c196fdb'),
        # Exactly one block long (2 blocks absorbed)
        Test(b"0"*(r//8), hash='d8fb234674bb6f52c5816813ed81cceabe0410af4903b0b218565ea8a49945c823305e47e8c7113d72d10b6a5a3a895ad5bc5de4e6842551d8014fb8a740ae08'),
        # Long message (multiple absorbtions)
        Test(b"abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ 01234567890 "*10, hash='86b7a7f84fda9727548a290a3f02a022176606f6864cdb07b331795a016d82ba3a3ee5069a7e8e814f49fa9ae0dfd1a8606f297b872c9142b7b8123ae73c8fe4'),
    ]
    for i, t in enumerate(tests):
        debug("Run test nr. {}".format(i), args.verbose)
        for k in keccak_impls:
            t.run(k, selftest=True)

    debug("========== Testing Keccak[128,272] ==========", args.verbose)
    r = 128 ; c = 272
    keccak_impls = [Keccak(rate=r, capacity=c, num_shares=x, dbg_level=args.verbose-1) for x in range(1, args.shares+1)]
    tests = [
        # Empty message
        Test(b"", hash='A19F1AA74311FFFE325B5157BB761E4633'),
        # Short message
        Test(b"hi", hash='A031BEDFD1BB1E0CC7B2B429C843BD3CC6'),
        # Exactly one block minus 1 byte long (only 1 block absorbed)
        Test(b"0"*(r//8-1), hash='867AFC2E246034C906000FEA5F9430E232'),
        # Exactly one block long (2 blocks absorbed)
        Test(b"0"*(r//8), hash='CF9DA11101E94A8590589E67D25F71E222'),
        # Long message (multiple absorbtions)
        Test(b"abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ 01234567890 "*10, hash='032A307700AD646762B7CF99F517E536B6'),
    ]
    for i, t in enumerate(tests):
        debug("Run test nr. {}".format(i), args.verbose)
        for k in keccak_impls:
            t.run(k, selftest=True)

def profile_hlm(args):
    import cProfile
    cProfile.run('test_hlm(args)')

def create_tests(args):
    random.seed(21) # yields 42 for random.choice(range(256)) the first time :-)
    r = args.rate ; c = args.capacity
    tests = [
        Test(b""),
        Test(b"hi"),
        Test(b"0"*(r//8-1)),
        Test(b"0"*(r//8)),
        Test(b"abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ 01234567890 "*10),
    ]
    k = Keccak(rate=args.rate, capacity=args.capacity, num_shares=args.shares, dbg_level=args.verbose-1)

    # Truncate existing file
    logfile = "{}/keccak_r{}_c{}_s{}.ref".format(args.directory[0], k.rate, k.capacity, k.num_shares)
    with open(logfile, 'w+') as _:
        pass

    for t in tests:
        t.run(k, logfile=logfile, selftest=False)

if __name__ == "__main__":
    # create the top level parser
    parser = argparse.ArgumentParser(description=None, epilog="Type the command name followed by --help to get help on that specific command")
    common_options = argparse.ArgumentParser(add_help=False)
    sp = parser.add_subparsers(title='commands')

    # Parent parser for 'selftest' and 'profile' commands. Options will be inherited
    common = argparse.ArgumentParser(add_help=False)
    common.add_argument('-v', '--verbose', action='count', default=0, help="Increase level of debug output for each given 'v'")

    # create the parser for the create-tests command
    sp_create = sp.add_parser('create-tests', parents=[common], help="Creates tests")
    sp_create.add_argument('-r', '--rate', default=128, type=int, help="Rate of the sponge construction")
    sp_create.add_argument('-c', '--capacity', default=272, type=int, help="Capacity of the sponge construction")
    sp_create.add_argument('-s', '--shares', default=2, type=int, help="Number of shares")
    sp_create.add_argument('-d', '--directory', nargs=1, type=str, default="created_tests", help="Directory for the created test files")
    sp_create.set_defaults(func=create_tests)

    # create the parser for the selftest command
    sp_selftest = sp.add_parser('selftest', parents=[common], help="Executes the tests for the HLM itself")
    sp_selftest.add_argument('-s', '--shares', default=5, type=int, help="Maximum number of shares used in the selftest")
    sp_selftest.set_defaults(func=test_hlm)

    # create the parser for the profile command
    sp_profile = sp.add_parser('profile', parents=[common], help="Executes the tests for the HLM and profiles the execution with cProfile")
    sp_profile.add_argument('-s', '--shares', default=5, type=int, help="Maximum number of shares used in the selftest")
    sp_profile.set_defaults(func=profile_hlm)

    # parse the arguments and call the appropriate function
    args = parser.parse_args()
    args.func(args)
