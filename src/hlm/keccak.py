from __future__ import print_function
from math import log
from bitarray import bitarray
import binascii, random

from keccak_interface import KeccakInterface

###############################################################################
# Utility functions

def rot(bitary, amount):
    amount = amount % bitary.length()
    result = bitary[amount:]
    result.extend(bitary[0:amount])
    return result

def tohex(bitary):
    return binascii.b2a_hex(bitary.tobytes())

def random_bytearray(length):
    r = range(256)
    bytes = bytearray(length)
    for i in range(len(bytes)):
        #TODO
        bytes[i] ^= 0 #random.choice(r)
    return bytes

###############################################################################

class Keccak(KeccakInterface):
    ROTATION_OFFSETS = [
        [0,  36,  3, 41, 18],
        [1,  44, 10, 45,  2],
        [62, 6,  43, 15, 61],
        [28, 55, 25, 21, 56],
        [27, 20, 39,  8, 14]
    ]

    RC = [bitarray() for _ in range(24)]
    [RC[i].frombytes(binascii.a2b_hex(rc)) for i, rc in enumerate([
        '0000000000000001', '0000000000008082', '800000000000808A', '8000000080008000',
        '000000000000808B', '0000000080000001', '8000000080008081', '8000000000008009',
        '000000000000008A', '0000000000000088', '0000000080008009', '000000008000000A',
        '000000008000808B', '800000000000008B', '8000000000008089', '8000000000008003',
        '8000000000008002', '8000000000000080', '000000000000800A', '800000008000000A',
        '8000000080008081', '8000000000008080', '0000000080000001', '8000000080008008'
    ])]

    def __init__(self, rate, capacity, num_shares, dbg_level):
        size = rate + capacity
        assert(size in [200,400,800,1600]) # [25,50,100] disallowed (<8 bit lane length)

        self._w = size//25
        self._rounds = 12 + 2*int(log(self._w,2))
        self.rate = rate
        self.capacity = capacity
        self.states = [[[bitarray('0'*self._w) for y in range(5)] for x in range(5)] for i in range(num_shares)]
        self.num_shares = num_shares
        self.logstring = ""
        self.dbg_level = dbg_level

        self.round_constants = [Keccak.RC[i][64-self._w:] for i in range(self._rounds)]
        #self.debug_states('init:\n')

    def reset(self):
        self.__init__(self.rate, self.capacity, self.num_shares, self.dbg_level)
        self.debug("reset")
        self.log("reset")

    ###########################################################################
    # Debugging and logging utilities

    def combine_shares(self):
        shares = self.states
        combined = [[bitarray('0'*self._w) for y in range(5)] for x in range(5)]
        for share in shares:
            for x in range(5):
                for y in range(5):
                    combined[x][y] ^= share[x][y]

        return combined

    def debug(self, msg):
        if self.dbg_level > 0:
            print(msg)

    def debug_states(self, msg):
        def print_state(state):
            for x in range(5):
                print("    lanes[{}][y0..y4] = ".format(x), end='')
                for y in range(5):
                    print("{}".format(tohex(state[x][y])), end='  ')
                print()
        if self.dbg_level > 0:
            print(msg, end='')
            if self.dbg_level > 1:
                for i,state in enumerate(self.states):
                    print("  Domain Nr {}:".format(i+1))
                    print_state(state)
                print("  Combined:")
            print_state(self.combine_shares())

    def log(self, msg):
        self.logstring += msg + '\n'

    def log_state(self, msg):
        state_string = msg + ' '
        state=self.combine_shares()
        for x in range(4,-1,-1):
            for y in range(4,-1,-1):
                state_string += "{}".format(tohex(state[x][y]))
        self.log(state_string)

    def write_log(self, file):
        file.writelines(self.logstring)

    ###########################################################################
    # Convenience function for message absorbtion

    def pad_message(self, message):
        M = bitarray(endian='big')
        M.frombytes(message + '\x01' )
        M.extend('0'*((self.rate - M.length()) % self.rate)) # zeros
        M[-8] ^= 1 # lastbyte xor 0x80
        assert(M.length() % self.rate == 0)
        return M.tobytes()

    def create_message_shares(self, message):
        bytes = bytearray(message)
        shares = [bytearray(len(bytes)) for _ in range(self.num_shares)]

        # Create N-1 random shares
        for i in range(self.num_shares-1):
            shares[i] = random_bytearray(len(bytes))

        # Last share is determined by xoring the data with all the random other shares
        last_share = bytes
        for share in shares:
            for j in range(len(bytes)):
                last_share[j] ^= share[j]
        shares[self.num_shares-1] = last_share

        return [str(share) for share in shares]

    def message_to_blocks(self, message_string, msg_is_padded=False):
        # Reverse the byte order: "hi\x01\80" -> "\x80\x01ih"
        M = bitarray()
        M.frombytes(message_string[::-1])

        # Split the array into lanes, then reverse the order since the first letter should be in the first lane and so on
        # e.g. Lanesize=2: "\x80\x01ih" -> ["\x80\x01", "ih"] -> ["ih", "\x80\x01"]
        lanes = [M[i:i+self._w] for i in range(0,M.length(),self._w)]
        lanes = lanes[::-1]

        # Arrange the lanes in blocks. The rate of the state determines the amount of lanes in a block
        # E.g. Lanesize=2, Rate=4, padded_message_length=8 -> 2 blocks of 2 lanes each
        lanes_in_block = self.rate//self._w
        blocks = [lanes[i:i+lanes_in_block] for i in range(0,len(lanes),lanes_in_block)]
        #debug("Message in blocks:\n{}".format([[tohex(lane) for lane in block] for block in blocks]))
        return blocks

    ###########################################################################
    # Keccak[r,c] sponge functions

    def absorb(self, message, msg_is_padded=False):
        # Pad message
        M = message
        if not msg_is_padded:
            M = self.pad_message(message)

        # Share message
        message_shares = self.create_message_shares(M)

        # Bring shares into correct format for absorbtion
        blocks_shares = [self.message_to_blocks(m) for m in message_shares]
        assert(len(blocks_shares) == self.num_shares)

        # Absorb
        rate = self.rate
        width = self._w
        num_blocks = len(blocks_shares[0])
        for block_nr in range(num_blocks):
            # Absorb one block of self.num_shares shares
            for share_nr in range(self.num_shares):
                S = self.states[share_nr]
                block = blocks_shares[share_nr][block_nr]
                logblock = ''
                for y in range(5):
                    for x in range(5):
                        if x+5*y < rate//width:
                            S[x][y] ^= block[x+5*y]
                            logblock += tohex(block[x+5*y])
                self.log("inputshare {} {}".format(logblock, share_nr))
            self.debug("message: '{}'".format(message))
            self.debug_states("absorbed:\n")
            self.log_state("absorb")
            self.f()

    def squeeze(self):
        # Reverse the byte order again (also see message_to_blocks)
        S = self.combine_shares()
        Z = ""
        for y in range(5):
            for x in range(5):
                Z += S[x][y].tobytes()[::-1]

        self.f()
        return binascii.b2a_hex(Z)

    ###########################################################################
    # Keccak-f round functions

    def f(self):
        for i in range(self._rounds):
            self.debug("-------------Round number {}-------------".format(i+1))
            self.theta()
            self.debug_states("theta:\n")
            self.log_state("theta")

            self.rho_pi()
            self.debug_states("rho_pi:\n")
            self.log_state("rho_pi")

            R = self.num_shares*(self.num_shares-1)//2
            Z = [bitarray() for _ in range(R)]
            [Z[j].frombytes(str(random_bytearray(self._w//8))) for j in range(R)]
            self.chi(randomness = Z)
            self.debug_states("chi:\n")
            self.log_state("chi")

            self.iota(i)
            self.debug_states("iota:\n")
            self.log_state("iota")

    def theta(self):
        for i in range(self.num_shares):
            A = self.states[i]
            C = [A[x][0] ^ A[x][1] ^ A[x][2] ^ A[x][3] ^ A[x][4] for x in range(5)]
            D = [C[(x-1) % 5] ^ rot(C[(x+1) % 5], 1) for x in range(5)]
            A = [[A[x][y] ^ D[x] for y in range(5)] for x in range(5)]
            self.states[i] = A

    def rho_pi(self):
        r = Keccak.ROTATION_OFFSETS
        for i in range(self.num_shares):
            A = self.states[i]
            B = [[bitarray('0'*self._w) for y in range(5)] for x in range(5)]
            for x in range(5):
                for y in range(5):
                    B[y][(2*x+3*y) % 5] = rot(A[x][y], r[x][y])
            self.states[i] = B

    def chi(self, randomness):
        assert(len(randomness) == (self.num_shares*(self.num_shares-1))//2)
        all_shares = self.states

        nextstates = [[[bitarray('0'*self._w) for y in range(5)] for x in range(5)] for i in range(self.num_shares)]
        w = self._w
        for x0 in range(5):
            x1 = (x0+1) % 5; x2 = (x0+2) % 5
            for y in range(5):
                for i, S in enumerate(all_shares):
                    A = bitarray('0'*w)
                    for j, T in enumerate(all_shares):
                        if j == i:
                            # part dependent on only one sharestate (inner domain part)
                            A ^= S[x0][y] ^ (~S[x1][y] & S[x2][y])
                        if j < i:
                            # part dependent on other sharestates (cross-domain parts)
                            Z = randomness[j+i*(i-1)//2]
                            A ^= (S[x1][y] & T[x2][y]) ^ Z
                        elif j > i:
                            # part dependent on other sharestates (cross-domain parts)
                            Z = randomness[i+j*(j-1)//2]
                            A ^= (S[x1][y] & T[x2][y]) ^ Z
                    nextstates[i][x0][y] = A

        #chi_ns = self.chi_non_shared()
        self.states = nextstates
        #assert(self.combine_shares() == chi_ns)

    def chi_non_shared(self):
        B = self.combine_shares()
        A = [[B[x][y] ^ (~B[(x+1) % 5][y] & B[(x+2) % 5][y]) for y in range(5)] for x in range(5)]
        return A

    def iota(self, round_nr):
        # Addition of the round constant to first share
        S = self.states[0]
        S[0][0] = S[0][0] ^ self.round_constants[round_nr]
